# Hausuebung5
Hausübung 5 in Python - Schätzen von Immobilienwerten


## Description
Künstliche Intelligenz zur Vorhersage von Immobiliendaten in 5 Schritten:

Voraussetzungen und benötigte Module/Bibliotheken
Datenimport der Rohdaten aus csv-Datei
Preprocessing (Datenvorbereitung und Skalierung)
Erstellung des Künstlichen Neuronalen Netzes und Training des KI-Modells
Testen der Genauigkeit der KI-Vorhersagen


## Installation
Jupyter Notebook Instanz des HRZ der TUDa -> https://tu-jupyter-iib.ca.hrz.tu-darmstadt.de/hub/home
(Jupyter Notebook und Python Installation notwendig)
+ pip install tensorflow
+ pip install keras

## Authors and acknowledgment
Idee und Code von http://python-programmieren.maximilianwittmann.de/kiselbstprogrammieren/


## Project status
Finished